# Curso QGIS básico

[TOC] 

## 01 ¿Qué es un Sistema de Información Geográfica?

De manera muy básica se puede decir que un Sistema de Información no es más que un lugar donde se almacenan datos y que consta de tres partes.

1. Lugar donde se guarda la información.
2. La información propiamente dicha.
3. Un índice donde se puede buscar qué información está disponible en el sistema para ser usada.

Uno de los ejemplos más comunes de este tipo de sistemas es una biblioteca, donde existe:

1. Un lugar donde se guarda información: estanterías donde se almacenan los libros y el propio edificio en sí.
2. Información guardada en el sistema: los libros que pueden encontrar en la biblioteca.
3. Un índice donde se puede encontrar los libros que hay disponibles. En la actualidad por lo general es un ordenador donde se puede consultar una base de datos que indica en qué estantería se encuentra cada libro.

La diferencia principal entre un Sistema de Información de este tipo y un Sistema de Información Geográfica es que en éste último los datos que se almacenan son de tipo geográfico y, en la actualidad, la inmensa mayoría de los mismos se encuentran en formato digital, por lo que además de las 3 partes anteriores hace falta una aplicación informática para poder ver y usar esta información.

De esta manera se podría decir que un Sistema de Información Geográfica (SIG) está formado por:

1. Un servidor donde se almacena la información geográfica.
2. La información propiamente dicha en los formatos necesarios para ser utilizada.
3. Una base de datos donde se puede buscar qué información geográfica se pone a disposición por el sistema.
4. Aplicaciones de acceso a la información geográfica que permitirán visualiza o editarla (QGIS, gvSIG, ArcGIS, GRASS, Saga...).

Existen muchos ejemplos de SIG que se utilizan prácticamente todos los días: Google Maps, navegador GPS del coche o móvil, páginas de información del estado del tráfico...

## 02 Tipos de aplicaciones SIG

### 02.1 Tipos de licencias

### 02.2 Aplicaciones SIG

## 03 Tipos de datos

## 04 Formatos de archivos SIG