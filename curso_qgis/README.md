# Temario curso QGIS

[TOC]

## 1. Introducción a los SIG

1. Qué es un SIG
   - Descripción de componentes
2. Tipos de datos SIG (vector / raster / alfanuméricos) y metadatos
3. Software abierto / privativo
   - Ventajas / inconvenientes
   - Por qué usamos software abierto
4. Fuentes de información espacial
   - Derechos de acceso e interoperabilidad
   - Datos abiertos / públicos
     - Concepto
     - Utilidades
     - Ventajas / inconvenientes
     - Formatos de datos abiertos
     - Importancia de la información actualizada

## 2. Toma de contacto con QGIS

1. Descripción de componentes
   - Tabla de contenidos
   - Lienzo (canvas)
   - Barras de herramientas
   - Explorador de información geográfica
   - Gestor de composiciones
   - Exportación (pdf, imagen)
2. Complementos
   - Búsqueda
   - Instalación

## 3. Trabajo básico con información espacial

1. Creación de información vectorial
   - shp
   - geopackage
   - Capas borrador
2. Carga de información en QGIS
   - Vectorial
     - shp
     - geopackage
     - kml
   - Raster
   - Alfanumérica
   - Servicios OGC
     - WMS
     - WFS
     - WCS
   - Conexiones XYZ
   - Complementos de carga de información de base
   - GPS (**nombrar, para curso avanzado**)
   - Bases de datos espaciales (**nombrar, para curso avanzado**)
   - Representar coordenadas desde una tabla alfanumérica
3. Sistemas de Referencia de Coordenadas
4. Exportar lienzo a pdf e imagen georreferenciada

## 4. Simbología y etiquetado

### 4.1 Simbología

1. Vectorial
   - Opciones de simbología vectorial (símbolo único, categorizado, graduado...)
   - Simbología definida por datos (**ver algún ejemplo, para curso avanzado**)
   - Efectos de dibujo (ver algún ejemplo, para curso avanzado)
   - Diferencias entre simbología desde la ventana de _Propiedades_ y el _Panel de estilo de capas_
2. Raster
   - Opciones de simbología raster (gris monobanda, pseudocolor monobanda...)
   - Transparencia
   - Histograma

### 4.2 Etiquetado

- Tipos de etiquetado
  - Etiquetas simples
  - Etiquetas basadas en reglas
- Opciones de etiquetado (texto, formateo, buffer...)
- Ubicación de las etiquetas
- Expresiones de etiquetado (**ver algún ejemplo, para curso avanzado**)
- Etiquetado definido por datos (**ver algún ejemplo, para curso avanzado**)

## 5. Análisis vectorial

1. Selección de elementos de una capa
   - Manualmente
   - Por valor
   - Por expresión
2. Herramientas de superposición vectorial
   - Cortar
   - Diferencia
   - Diferencia simétrica
   - Intersección
   - Unión
   - Buffer
   - Generalizar (disolver)
3. Caja de herramientas de procesos
   - Mostrar y usar
   - Enseñar cosas para curso avanzado (análisis de redes, bases de datos...)
4. Cálculos
   - Superficie
   - Estadísticas de capa
   - Estadísticas de campos
   - Porcentajes, densidades... (ejemplos)
5. Guardado de capa existente (completa y elementos seleccionados)
6. Reproyección
   - Al vuelo
   - Permanente
7. Trabajo con tablas de atributos
   - Selección de registros
     - Manualmente
     - Por expresión
   - Filtros de registros
   - Calculadora de campos
     - Opciones
     - Ejemplos
     - Campos virtuales (**nombrar, para curso avanzado**)
8. Scripting en QGIS (**explicar, para curso avanzado**)

## 6. Análisis raster

- Obtención de estadísticas de capa
  - Desde ventana de propiedades
  - Desde caja de herramientas de procesos
  - Estadísticas de zona
- Análisis del terreno
  - Pendientes
  - Orientación
  - Sombras
- Calculadora raster
  - Recortes
    - Capa
    - Extensión
  - Ejemplos de cálculos
    - Zonas con valores máximos y mínimos
    - Zonas de valores mayores y menores de un umbral
    - Reclasificación de capas raster
    - Cálculo de contornos

## 7. Salidas gráficas

1. Propiedades de la página
2. Diseño
   - Añadir mapa
   - Imágenes
   - Texto
   - Escala (numérica y gráfica)
   - Rosa de los vientos
   - Zoom (al folio y al contenido)
3. Leyenda
4. Organización de elementos del plano / mapa
5. Guías
6. Plantillas
7. Exportar salida (formatos)